// API Routes
const express = require('express');
const router = express.Router();
const Item = require('../models/Item');


// POST 

router.post('/post', async (req, res) => {
    const item = new Item({
        itemName: req.body.itemName,
        expDate: req.body.expDate,
        quantity: req.body.quantity
    });
    try{
        const newItem = await item.save();
        res.status(201).json(newItem);
    }catch(err){
        res.status(204).json({message: err});
    }
});

// GET

router.get('/get', async (req, res) => {
    try{
        const items = await Item.find();
        res.status(200).json(items);
    }catch(err){
        res.status(404).json({message:err});
    }
});

// GET item by ID

router.get('/get/:itemID', async (req, res) => {
    try{
        const specificItem = await Item.findById(req.params.itemID);
        res.status(200).json(specificItem);
    }catch(err){
        res.status(404).json({message:err});
    }
});

// GET item by name

router.get('/get/name/:itemName', async (req, res) => {
    try{
        const items = await Item.find({"name": req.params.itemName.toLowerCase()});
        res.status(200).json(items);
    }catch(err){
        res.status(404).json({message:err});
    }
});

// GET item by expiration date

router.get('/get/date/:expDate', async (req, res) => {
    try{
        const items = await Item.find({"expDate": req.params.expDate});
        res.status(200).json(items);
    }catch(err){
        res.status(404).json({message:err});
    }
});

// DELETE by itemID

router.delete('/delete/:itemID', async (req, res) => {
    try{
        const specificItem = await Order.findByIdAndDelete(req.params.itemID);
        res.status(200).json(specificItem);
    }catch(err){
        res.status(404).json({message:err});
    }
});

module.exports = router;
