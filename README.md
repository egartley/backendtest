# Inventory System Backend

The backend of the Inventory System for the Bear Necessities Market. The code for the backend of the Inventory System will live in this repository. The backend will hold the database that originally pulls information from an excel sheet in a shared email. Eventually the database will be updated through employees using a GUI located in the frontend. The database will hold items and number of those items available.

## Installation
```bash
# Clone the project
$ git clone https://gitlab.com/LibreFoodPantry/client-solutions/bear-necessities-market/inventorysystem/backend

# Change directory to project
$ cd backend

# Intended run of the project
$ docker-compose up
```

## Usage
```bash
# When docker is running

# Build server
$ ./build-backup-server.sh

# Start up server
$ ./up.sh

# Build docker image
$ ./build.sh

# Shut down server
$ ./down.sh
```

## Tools

The following tools are intended to be used in the backend of the Inventory Systems:
- [MongoDB](https://www.mongodb.com/)
- [Mongoose](https://mongoosejs.com/)
- [Node.js](https://nodejs.org/en/)
- [JavaScript](https://www.javascript.com/)
- [Express](https://expressjs.com/)

## Creating a MongoDB Instance in Docker
This is a step-by-step process to create a MongoDB Instance. This example shows how to create a MongoDB instance that can be used for current database and as a test database.

```
docker images
(checks to see the images that are downloaded on the computer. I used this to double check if I had the mongo image downloaded)

docker pull mongo:latest
(Checks to see if latest version of mongo is installed, otherwise it will download the latest version. NOTE: You can also specify which version you would like to work with. This is useful when multiple team members are working on the database.)

mkdir mongodb-inventory-system
(Creates a directory specific to mongodb. NOTE: 'mongodb-inventory-system' can be replaced with whatever you would like to name your directory.)

cd mongodb-inventory-system
(Access the directory created)

docker run -d -p 27017:27017 -v ~/mongodb-inventory-system:/data/db --name testmongo mongo:latest
(Runs/creates the mongodb container in docker include the directory name as shown above and the phrase "testmongo" is simply the name I decided to call the container itself. -d simply means we are running a datched or 'background version')

docker ps
(Lists the running containers by default)

docker exec -it testmongo bash
(Part 1: Will connect/execute to our deployment named testmongo using the interative terminal and start the bash shell)

mongo
(PArt 2: After executing the container, you simply write in mongo next to the /# and it will launch the MongoDB shell client)

show dbs
(This shows which databases exist in the instance)

use InventorySystemTest
(The command use along with a name of your choice will create a new database)

Example on adding information:

db.people.save({ firstname: "Nic", lastname: "Raboy" })

People is a new collection created in Mongodb
```
## License
[GNU General Public License](https://www.gnu.org/licenses/gpl-3.0.en.html)
