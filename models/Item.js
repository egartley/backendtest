const mongoose = require('mongoose');

const ItemSchema = mongoose.Schema({
    itemName: String,
    expDate: Date,
    quantity: Number
});

module.exports = ItemSchema;