const axios = require('./axios.js');

module.exports = {
    async listItems() {
        return axios.get("/inventory");
    },

    async createItem(data) {
        if (data === undefined) {
            data = { "name": "clown" };
        }
        if (data === null) {
            return axios.post("/inventory");
        } else {
            return axios.post("/inventory", data);
        }
    },

    async viewItem(id) {
        return axios.get("/inventory/" + id);
    },

    async updateItem(item) {
        return axios.put("/inventory/" + item._id, item);
    },

    async deleteItem(id) {
        return axios.delete("/inventory/" + id);
    },
};
