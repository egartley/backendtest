#!/usr/bin/env bash

set -ue -o pipefail -o posix

docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"

if ! docker pull "$PIPELINE_IMAGE_NAME" &> /dev/null ; then
    echo "PIPELINE_IMAGE_NAME=$PIPELINE_IMAGE_NAME"
    echo "Docker image not found. No image pulled."
else

    if [ -z "$IMAGE_NAME" ] ; then
        echo "ERROR: IMAGE_NAME is not defined."
        exit 1
    fi

    docker tag "$PIPELINE_IMAGE_NAME" "$IMAGE_NAME"
fi

echo "TEST_COMMAND would be run here (currently not implemented)"
