// Database Instance
const mongoose = require('mongoose');
const express = require('express');
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');
const path = require('path');
const inventoryRoutes = require('../routes/items');



app.use(cors());
app.use(bodyParser.json());

app.get('/api', (req, res) => {
    res.status(200).send('Inventory System API');
});

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname + 'localhost:3000'));
});

app.use(inventoryRoutes);

mongoose.connect('mongodb://localhost:27017/Inventory');
const database = mongoose.connection;

database.once('open', () => {
    console.log(database.readyState);
});